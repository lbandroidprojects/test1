package com.example.lukaszb.test1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener{
    private Button b2;
    private Button b3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b2=findViewById(R.id.button2);
        b2.setOnClickListener(this);
        b3=findViewById(R.id.button3);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(),"Kliknięto przycisk 3",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onButtonClick(View view){
        Toast.makeText(this,"Kliknięto przycisk 1",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(this,"Kliknięto przycisk 2",
                Toast.LENGTH_SHORT).show();
    }
}
